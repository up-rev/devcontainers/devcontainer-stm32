#@file Makefile
#@brief makefile for containerized builds of project artifacts 
#@author Jason Berger
#@date 02/20/2023
  

#Project
PROJECT_NAME ?= my-project
BUILD_CONFIG ?= Release
PROJ_DIR ?= .
DEVCONTAINER_IMAGE ?= uprev/stm32



.PHONY: docker-mount docker-build native-build

#Default is a passthrough to call targets in the default makefile
.DEFAULT:
	docker run --mount src=$(PWD),target=/workspace,type=bind -w /workspace $(DEVCONTAINER_IMAGE) \
	make $@


native-build: 
	/opt/stm32cubeide/stm32cubeide -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild -import $(PROJ_DIR) -build $(PROJECT_NAME)_CM4/$(BUILD_CONFIG)

docker-build: 
	docker run --mount src=$(PWD),target=/workspace,type=bind -w /workspace $(DEVCONTAINER_IMAGE) \
	/opt/stm32cubeide/stm32cubeide -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild -import $(PROJ_DIR) -build $(PROJECT_NAME)_CM4/$(BUILD_CONFIG)
	


