FROM wsbu/stm32cubeide:1.5.0 as dev_stage


ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y --fix-missing --no-install-recommends \ 
    wget \ 
    cmake \
    build-essential \
    openssl \
    make \ 
    git \
    python3 \
    python3-pip \
    python3-setuptools \
    texlive \
    latexmk \
    texlive-science \
    texlive-formats-extra \ 
    tex-gyre

#sphinx dependencies 
RUN pip3 install sphinx sphinxcontrib-plantuml sphinx-rtd-theme

#mrtutils 
RUN pip3 install mrtutils
